<?php get_header(); ?>
	<?php
		// Check and get Sidebar Class
		$sidebar = get_post_meta($post->ID,'post-option-sidebar-template',true);
		if( empty($sidebar) ){
			global $default_post_sidebar;
			$sidebar = $default_post_sidebar; 
		}		
		$sidebar_array = gdl_get_sidebar_size( $sidebar );

		// Translator words
		if( $gdl_admin_translator == 'enable' ){
			$translator_about_author = get_option(THEME_SHORT_NAME.'_translator_about_author', 'About the Author');
			$translator_social_share = get_option(THEME_SHORT_NAME.'_translator_social_shares', 'Social Share');
		}else{
			$translator_about_author = __('About the Author','gdl_front_end');
			$translator_social_share = __('Social Share','gdl_front_end');
		}
	?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="page-wrapper single-blog <?php echo $sidebar_array['sidebar_class']; ?>">
		<?php
			global $left_sidebar, $right_sidebar, $default_post_left_sidebar, $default_post_right_sidebar;
			$left_sidebar = get_post_meta( $post->ID , "post-option-choose-left-sidebar", true);
			$right_sidebar = get_post_meta( $post->ID , "post-option-choose-right-sidebar", true);
			if( empty( $left_sidebar )){ $left_sidebar = $default_post_left_sidebar; } 
			if( empty( $right_sidebar )){ $right_sidebar = $default_post_right_sidebar; } 
			
			global $blog_single_size, $sidebar_type;
			$item_size = $blog_single_size[$sidebar_type];
	
			// starting the content
			echo '<div class="row gdl-page-row-wrapper">';
			echo '<div class="gdl-page-left mb0 ' . $sidebar_array['page_left_class'] . '">';
			
			echo '<div class="row">';
			echo '<div class="gdl-page-item mb0 pb20 gdl-blog-full ' . $sidebar_array['page_item_class'] . '">';
			if ( have_posts() ){
				while (have_posts()){
					the_post();

					// blog thumbnail
					print_single_blog_thumbnail( get_the_ID(), $item_size );
					
					echo '<div class="blog-content-wrapper">';
					
					// blog title
					echo '<h1 class="blog-title"><a href="' . get_permalink() . '">' . get_the_title() . '</a></h1>';

					// blog information
					echo '<div class="blog-info-wrapper">';
					echo '<div class="blog-date"><i class="icon-calendar"></i>';
					echo '<a href="' . get_day_link( get_the_time('Y'), get_the_time('m'), get_the_time('d')) . '" >';
					echo get_the_time($gdl_date_format);
					echo '</a>';
					echo '</div>';			

					echo '<div class="blog-comment"><i class="icon-comments"></i>';
					comments_popup_link( __('0 Komentāri','gdl_front_end'),
						__('1 Komentārs','gdl_front_end'),
						__('% Komentāri','gdl_front_end'), '',
						__('Komentāri ir izslēgti','gdl_front_end') );
					echo '</div>';						

					echo '<div class="blog-author"><i class="icon-user"></i>';
					echo the_author_posts_link();
					echo '</div>';	
					
					$tags_opening = '<div class="blog-tag"><i class="icon-tags"></i>';
					$tags_ending = '</div>';
					the_tags( $tags_opening, ', ', $tags_ending );


					
					echo '<div class="clear"></div>';
					echo '</div>'; // blog information
					//if(get_post_meta($post->ID, 'post-option-social-enabled', true) != "No"){
					//	include_social_shares();
					//	echo '<div style="margin-top:27px;"><div class="g-plusone"></div></div>';
					//	echo "<div class='clear'></div>";
					//}
					// blog content
					echo '<div class="blog-content">';
					the_content();
					wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'gdl_front_end' ) . '</span>', 'after' => '</div>' ) );
					echo '<div class="clear"></div>';
					echo '</div>';

					// About Author
					/*if(get_post_meta($post->ID, 'post-option-author-info-enabled', true) != "No"){
						echo "<div class='about-author-wrapper'>";
						echo "<div class='about-author-avartar'>" . get_avatar( get_the_author_meta('ID'), 90 ) . "</div>";
						echo "<div class='about-author-info'>";
						echo "<h5 class='about-author-title'>" . $translator_about_author . "</h5>";
						echo get_the_author_meta('description');
						echo "</div>";
						echo "<div class='clear'></div>";
						echo "</div>";
					}*/

					
					// Include Social Shares
					//if(get_post_meta($post->ID, 'post-option-social-enabled', true) != "No"){
						//echo "<h3 class='social-share-title'>" . $translator_social_share . '</h3>';
					//	include_social_shares();
					//	echo '<div style="margin-top:7px;"><div class="g-plusone"></div></div>';
					//	echo "<div class='clear'></div>";
					//}
					// subsribe to our news //
					/*echo '<h3 class="gdl-header-title">Saņem infografiku "14 veiksmīgas mājas lapas elementi"<br/> piesakoties jaunākajiem rakstiem."</h3><br/><br/>';
					echo do_shortcode( '[sbscrbr_form]' );*/
				
					//echo '<div class="comment-wrapper">';
					//comments_template(); 
					//echo '</div>';
					/*$content='
					<div class="benefit-icons">
					[column col="1/3"]
					<img src="http://stratcom.lv/wp-content/uploads/2015/01/ml-izstrade.png"></img>
					<h3 class="blogh3">Mājas lapu</h3>
					<p>izstrāde / uzstādīšana</p>
					<div class="column-service-button">
					<a class="gdl-button" href="http://stratcom.lv/majas-lapu-izstrade/">Uzzini vairāk</a>
					</div>
					[/column]

					[column col="1/3"]
					<img src="http://stratcom.lv/wp-content/uploads/2015/01/klientu-piesaiste.png"></img>
					<h3 class="blogh3">Klientu</h3>
					<p>Piesaiste</p>
					<div class="column-service-button">
					<a class="gdl-button" href="http://stratcom.lv/klientu-piesaiste/">Uzzini vairāk</a>
					</div>
					[/column]

					[column col="1/3" last="true"]
					<img src="http://stratcom.lv/wp-content/uploads/2015/01/ml-uzlabosana.png"></img>
					<h3 class="blogh3">Mājas lapu</h3>
					<p>Uzlabošana</p>
					<div class="column-service-button">
					<a class="gdl-button" href="http://stratcom.lv/majas-lapu-uzlabosana/">Uzzini vairāk</a>
					</div>
					</div>
					[/column]';*/

					echo do_shortcode( $content );
					echo '</div>'; // blog content wrapper
				}
			}
			echo "</div>"; // end of gdl-page-item
			
			get_sidebar('left');	
			echo '<div class="clear"></div>';			
			echo "</div>"; // row
			echo "</div>"; // gdl-page-left

			get_sidebar('right');
			echo '<div class="clear"></div>';
			echo "</div>"; // row
		?>
		<div class="clear"></div>
	</div> <!-- page wrapper -->
	</div> <!-- post class -->
	<script src="https://apis.google.com/js/platform.js" async defer>
	  {lang: 'lv'}
	</script>

<?php get_footer(); ?>