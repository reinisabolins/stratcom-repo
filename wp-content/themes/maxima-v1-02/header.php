<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>

	<!-- Basic Page Needs
  ================================================== -->
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<title><?php wp_title(); ?></title>
	<script src="//cdn.optimizely.com/js/2638170622.js"></script>
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- CSS
  ================================================== -->
	<?php global $gdl_is_responsive; ?>
	<?php if( $gdl_is_responsive ){ ?>
		<meta name="viewport" content="width=device-width, user-scalable=no">
	<?php } ?>
	
	<!--[if IE 7]>
		<link rel="stylesheet" href="<?php echo GOODLAYERS_PATH; ?>/stylesheet/ie7-style.css" /> 
		<link rel="stylesheet" href="<?php echo GOODLAYERS_PATH; ?>/stylesheet/font-awesome/font-awesome-ie7.min.css" /> 
	<![endif]-->	
	
	<?php
		
		// include favicon in the header
		if(get_option( THEME_SHORT_NAME.'_enable_favicon','disable') == "enable"){
			$gdl_favicon = get_option(THEME_SHORT_NAME.'_favicon_image');
			if( $gdl_favicon ){
				$gdl_favicon = wp_get_attachment_image_src($gdl_favicon, 'full');
				echo '<link rel="shortcut icon" href="' . $gdl_favicon[0] . '" type="image/x-icon" />';
			}
		} 
		
		// add facebook thumbnail to this page
		$thumbnail_id = get_post_thumbnail_id();
		if( !empty($thumbnail_id) ){
			$thumbnail = wp_get_attachment_image_src( $thumbnail_id , '150x150' );
			echo '<meta property="og:image" content="' . $thumbnail[0] . '"/>';		
		}
		
		// start calling header script
				

	?>



<!-- garumziimju skripts -->
<script type="text/javascript">
  WebFontConfig = {
    google: { families: [ 'Open+Sans:400,300,700:latin,latin-ext' ] }
  };
  (function() {
    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
      '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
  })(); 
  </script>
  <!-- //garumzīmju skripts -->

  <!-- google + -->
  <script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'lv'}
  </script>
  <!-- /google + -->

  
<!-- End Visual Website Optimizer Asynchronous Code -->
<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<?php wp_head(); ?>

    
<script type="text/javascript">

var hasBeenClicked = false;
$('#id').click(function () {
    hasBeenClicked = true;
getPage();
});

function getPage(id){

	$('#output').html('<img style="position:absolute; margin-lef:300px; left:40%;" src="http://stratcom.lv/wp-content/uploads/2015/05/infinity-1.gif" />');
	jQuery.ajax({
		url: "/getportf.php",
		data:'idval='+id,
		type: "POST",

		success:function(data){$('#output').html(data);
	}

	});
}

if (!hasBeenClicked) {

var id = 0;
	$('#output').html('<img style="position:absolute; margin-lef:300px; left:40%;" src="http://stratcom.lv/wp-content/uploads/2015/05/infinity-1.gif" />');
	jQuery.ajax({
		url: "/getportf.php",
		data:'idval='+id,
		type: "POST",

		success:function(data){$('#output').html(data);
}

	});

}

	</script>






</head>
<body <?php echo body_class(); ?>>

<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MWGLRV"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MWGLRV');</script>
<!-- facebook plugin -->

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=702503016500410&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- facebook plugin end -->
<?php
	// print custom background
	$background_style = get_option(THEME_SHORT_NAME.'_background_style', 'Pattern');
	if($background_style == 'Custom Image'){
		$background_id = get_option(THEME_SHORT_NAME.'_background_custom');
		$alt_text = get_post_meta($background_id , '_wp_attachment_image_alt', true);
		
		if(!empty($background_id)){
			$background_image = wp_get_attachment_image_src( $background_id, 'full' );
			echo '<div class="gdl-custom-full-background">';
			echo '<img src="' . $background_image[0] . '" alt="' . $alt_text . '" />';
			echo '</div>';
		}
	}
	
	// floating nav
	if( get_option(THEME_SHORT_NAME.'_enable_floating_menu', 'enable') == 'enable' ){
		echo '<div class="floating-nav-wrapper">';
		echo '<span class="subscribe">'. do_shortcode( '[AnythingPopup id="1"]' ).'</span>';
		wp_nav_menu( array('container' => '', 'menu_class'=> 'sf-menu',  'theme_location' => 'main_menu' ) );
		echo '</div>';
	}
?>
<div class="body-outer-wrapper">
	<div class="body-wrapper">
		<div class="header-outer-wrapper">
			<!-- top navigation -->
			<?php if( get_option(THEME_SHORT_NAME.'_enable_top_bar' ,true) == 'enable'){ ?>
				<div class="top-navigation-wrapper boxed-style">
					<div class="top-navigation-container container">
						<?php

							if( get_option(THEME_SHORT_NAME.'_enable_top_search', 'enable') == 'enable' ){
								echo '<div class="top-search-wrapper">'; 
								?>
								<div class="gdl-search-form">
									<form method="get" id="searchform" action="<?php  echo home_url(); ?>/">
										<input type="submit" id="searchsubmit" value="" />
										<div class="search-text" id="search-text">
											<input type="text" value="" name="s" id="s" autocomplete="off" data-default="<?php echo $search_val; ?>" />
										</div>
										<div class="clear"></div>
									</form>
								</div>
								<?php
								echo '</div>';
							}
						
							$top_left_text = get_option(THEME_SHORT_NAME.'_top_navigation_left');
							if( !empty($top_left_text) ){
								echo '<div class="top-navigation-left">';
								echo do_shortcode( __( $top_left_text , 'gdl_front_end') );
								echo '</div>';
							}			

							echo '<div class="top-navigation-right">';
							//echo '<div class="g-plusone" data-size="tall" data-annotation="inline" data-width="100"></div>';
							
							// Get Social Icons
							$gdl_social_icon = array(
								'deviantart'=> array('name'=>THEME_SHORT_NAME.'_deviantart', 'url'=> GOODLAYERS_PATH.'/images/icon/social-icon/deviantart.png'),
								'digg'=> array('name'=>THEME_SHORT_NAME.'_digg', 'url'=> GOODLAYERS_PATH.'/images/icon/social-icon/digg.png'),
								'facebook' => array('name'=>THEME_SHORT_NAME.'_facebook', 'url'=> GOODLAYERS_PATH.'/images/icon/social-icon/facebook.png'),
								'flickr' => array('name'=>THEME_SHORT_NAME.'_flickr', 'url'=> GOODLAYERS_PATH.'/images/icon/social-icon/flickr.png'),
								'lastfm'=> array('name'=>THEME_SHORT_NAME.'_lastfm', 'url'=> GOODLAYERS_PATH.'/images/icon/social-icon/lastfm.png'),
								'linkedin' => array('name'=>THEME_SHORT_NAME.'_linkedin', 'url'=> GOODLAYERS_PATH.'/images/icon/social-icon/linkedin.png'),
								'picasa'=> array('name'=>THEME_SHORT_NAME.'_picasa', 'url'=> GOODLAYERS_PATH.'/images/icon/social-icon/picasa.png'),
								'rss'=> array('name'=>THEME_SHORT_NAME.'_rss', 'url'=> GOODLAYERS_PATH.'/images/icon/social-icon/rss.png'),
								'stumble-upon'=> array('name'=>THEME_SHORT_NAME.'_stumble_upon', 'url'=> GOODLAYERS_PATH.'/images/icon/social-icon/stumble-upon.png'),
								'tumblr'=> array('name'=>THEME_SHORT_NAME.'_tumblr', 'url'=> GOODLAYERS_PATH.'/images/icon/social-icon/tumblr.png'),
								'twitter' => array('name'=>THEME_SHORT_NAME.'_twitter', 'url'=> GOODLAYERS_PATH.'/images/icon/social-icon/twitter.png'),
								'vimeo' => array('name'=>THEME_SHORT_NAME.'_vimeo', 'url'=> GOODLAYERS_PATH.'/images/icon/social-icon/vimeo.png'),
								'youtube' => array('name'=>THEME_SHORT_NAME.'_youtube', 'url'=> GOODLAYERS_PATH.'/images/icon/social-icon/youtube.png'),
								'google_plus' => array('name'=>THEME_SHORT_NAME.'_google_plus', 'url'=> GOODLAYERS_PATH.'/images/icon/social-icon/google-plus.png'),
								'email' => array('name'=>THEME_SHORT_NAME.'_email', 'url'=> GOODLAYERS_PATH.'/images/icon/social-icon/email.png'),
								'pinterest' => array('name'=>THEME_SHORT_NAME.'_pinterest', 'url'=> GOODLAYERS_PATH.'/images/icon/social-icon/pinterest.png')
							);				
							
							echo '<div id="gdl-social-icon" class="social-wrapper gdl-retina">';
							echo '<div class="social-icon-wrapper">';
							foreach( $gdl_social_icon as $social_name => $social_icon ){
								$social_link = get_option($social_icon['name']);
								
								if( !empty($social_link) ){
									echo '<div class="social-icon"><a target="_blank" href="' . $social_link . '">' ;
									echo '<img src="' . $social_icon['url'] . '" alt="' . $social_name . '" width="18" height="18" />';
									echo '</a></div>';
								}
							}
							echo '</div>'; // social icon wrapper
							echo '</div>'; // social wrapper								
							echo '</div>';
						?>
						<div class="clear"></div>
					</div>
				</div> <!-- top navigation wrapper -->
			<?php } ?>
			
			<div class="header-wrapper boxed-style">
				<div class="header-container container">
					<!-- Get Logo -->
					<div class="logo-wrapper">
						<?php
							$logo_id = get_option(THEME_SHORT_NAME.'_logo');
							if( empty($logo_id) ){	
								$alt_text = 'default-logo';	
								$logo_attachment = GOODLAYERS_PATH . '/images/default-logo.png';
							}else{
								$alt_text = get_post_meta($logo_id , '_wp_attachment_image_alt', true);	
								$logo_attachment = wp_get_attachment_image_src($logo_id, 'full');
								$logo_attachment = $logo_attachment[0];
							}

							if( is_front_page() ){
								echo '<h1><a href="'; 
								echo home_url();
								echo '"><img src="' . $logo_attachment . '" alt="' . $alt_text . '"/></a></h1>';	
							}else{
								echo '<a href="'; 
								echo home_url();
								echo '"><img src="' . $logo_attachment . '" alt="' . $alt_text . '"/></a>';				
							}
						?>
					</div>

					<!-- Navigation -->
					<div class="gdl-navigation-wrapper">
						<?php 

							// responsive menu
							if( $gdl_is_responsive && has_nav_menu('main_menu') ){
								dropdown_menu( array('dropdown_title' => '-- Main Menu --', 'indent_string' => '- ', 'indent_after' => '','container' => 'div', 'container_class' => 'responsive-menu-wrapper', 'theme_location'=>'main_menu') );	
								echo '<div class="clear"></div>';
							}
							/*echo '<div style="float:right;"><img style="padding: 7px;" src="http://stratcom.lv/wp-content/uploads/2015/03/phone-small.png"/><p class="tel">+371 28878623</p></div>';*/
							// main menu
							$sliding_bar = (get_option(THEME_SHORT_NAME.'_enable_sliding_bar', 'enable') == 'enable')? 'sliding-bar': '';
							
							echo '<div class="navigation-wrapper ' . $sliding_bar . '">';
							if( has_nav_menu('main_menu') ){
								echo '<div class="main-superfish-wrapper" id="main-superfish-wrapper" >';
								wp_nav_menu( array('container' => '', 'menu_class'=> 'sf-menu',  'theme_location' => 'main_menu' ) );
								echo '<div class="clear"></div>';
								echo '</div>';
								
								if( !empty($sliding_bar) ){
									echo '<div class="gdl-current-menu" ></div>'; 
								}
							}
							echo '<div class="clear"></div>';
							echo '</div>'; // navigation-wrapper 
						?>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</div> <!-- header container -->
			</div> <!-- header wrapper -->
		</div> <!-- header outer wrapper -->
		<?php

			if( is_page() ){
				// Top Slider Part
				global $gdl_top_slider_xml, $gdl_top_slider_type;
				$full_slider = get_post_meta($post->ID, 'page-option-enable-full-slider', true);
				$full_slider = ($full_slider == 'Yes')? 'full-slider': 'container-slider';
				
				if( $gdl_top_slider_type == 'Layer Slider' ){
					$layer_slider_id = get_post_meta( $post->ID, 'page-option-layer-slider-id', true);
					echo '<div class="gdl-top-slider">';
					echo '<div class="gdl-top-slider-wrapper ' . $full_slider . '">';
					echo do_shortcode('[layerslider id="' . $layer_slider_id . '"]');
					echo '<div class="clear"></div>';
					echo '<div class="page-title-top-shadow"></div>';
					echo '</div>';
					echo '</div>';
				}else if( empty($gdl_top_slider_type) || $gdl_top_slider_type == 'Title' || $gdl_top_slider_type == 'No Slider' ){
					$page_caption = get_post_meta($post->ID, 'page-option-caption', true);
					print_page_header(get_the_title(), $page_caption);					
				}else if ( $gdl_top_slider_type != "None"){
					echo '<div class="gdl-top-slider">';
					echo '<div class="gdl-top-slider-wrapper ' . $full_slider . '">';			
					$slider_xml = "<Slider>" . create_xml_tag('size', 'full-width');
					$slider_xml = $slider_xml . create_xml_tag('slider-type', $gdl_top_slider_type);
					$slider_xml = $slider_xml . $gdl_top_slider_xml;
					$slider_xml = $slider_xml . "</Slider>";
					$slider_xml_dom = new DOMDocument();
					$slider_xml_dom->loadXML($slider_xml);
					print_slider_item($slider_xml_dom->documentElement);
					echo '<div class="clear"></div>';
					echo '<div class="page-title-top-shadow"></div>';
					echo '</div>';
					echo '</div>';
				}	
			}else if( is_single() ){
				if( $post->post_type == 'portfolio' ){
					$single_title = get_the_title();
					$single_caption = get_post_meta( $post->ID, "post-option-blog-header-caption", true);
					print_page_header($single_title, $single_caption);					
				}else{
					$single_title = get_post_meta( $post->ID, "post-option-blog-header-title", true);
					$single_caption = get_post_meta( $post->ID, "post-option-blog-header-caption", true);
					if(empty( $single_title )){
						$single_title = get_option(THEME_SHORT_NAME . '_default_post_header','Blog Post');
						$single_caption = get_option(THEME_SHORT_NAME . '_default_post_caption');
					}
					print_page_header($single_title, $single_caption);			
				}	
			}else if( is_404() ){
				global $gdl_admin_translator;
				if( $gdl_admin_translator == 'enable' ){
					$translator_404_title = get_option(THEME_SHORT_NAME.'_404_title', 'Page Not Found');
				}else{
					$translator_404_title = __('Page Not Found','gdl_front_end');		
				}			
				print_page_header($translator_404_title);
			}else if( is_search() ){
				global $gdl_admin_translator;
				if( $gdl_admin_translator == 'enable' ){
					$title = get_option(THEME_SHORT_NAME.'_search_header_title', 'Search Results');
				}else{
					$title = __('Search Results', 'gdl_front_end');
				}		
				
				$caption = get_search_query();
				print_page_header($title, $caption);
			}else if( is_archive() ){
				
				if( is_category() || is_tax('portfolio-category') || is_tax('product_cat') ){
					$title = __('Category','gdl_front_end');
					$caption = single_cat_title('', false);
				}else if( is_tag() || is_tax('portfolio-tag') || is_tax('product_tag') ){
					$title = __('Tag','gdl_front_end');
					$caption = single_cat_title('', false);
				}else if( is_day() ){
					$title = __('Day','gdl_front_end');
					$caption = get_the_date('F j, Y');
				}else if( is_month() ){
					$title = __('Month','gdl_front_end');
					$caption = get_the_date('F Y');
				}else if( is_year() ){
					$title = __('Year','gdl_front_end');
					$caption = get_the_date('Y');
				}else if( is_author() ){
					$title = __('By','gdl_front_end');
					
					$author_id = get_query_var('author');
					$author = get_user_by('id', $author_id);
					$caption = $author->display_name;					
				}else{
					$title = __('Shop','gdl_front_end');
				}
						
				print_page_header($title, $caption);				
			} 
		?>
		<div class="content-outer-wrapper">
			<div class="content-wrapper container main ">