<div class="<?php get_row_class(); ?> center">
	<span class="sun"></span>
	<span class="cloud"></span>
<?php
    $counter = 0;
    foreach( $concepts as $concept ) {
        
        $class = 'col-md-2';
        $animation = '';
        
        if( 0 == $counter)
            $class .= ' col-md-offset-1';
        else {
            $animation = ' data-appear-animation-delay="' . ( 200 * $counter ) . '"';
        }
        
        echo '
        <div class="' . $class . '">
    		<div class="process-image" data-appear-animation="bounceIn"' . $animation . '>
                <img src="' . $concept['img'] . '" alt="" />
    			<strong>' . $concept['title'] . '</strong>
    		</div>
    	</div>';
        
        $counter++;
    }
    
    if( !empty( $works ) ) {
        
        $js = '
        // Circle Slider
    	if($("#fcSlideshow").get(0)) {
    		$("#fcSlideshow").flipshow();
    
    		setInterval( function() {
    			$("#fcSlideshow div.fc-right span:first").click();
    		}, 3000);
    
    	}
        
        Core.moveCloud();
        
/*
  $( ".par2" ).hide();
  $( ".par3" ).hide();
  $( ".par4" ).hide();
  $( ".par5" ).hide();
  $(".circ.a").click(function(){
    $( ".par1" ).show();
    $( ".par2" ).hide("slow");
    $( ".par3" ).hide("slow");
    $( ".par4" ).hide("slow");
    $( ".par5" ).hide("slow");
    $( ".circ.a" ).addClass( "active" );
    $( ".circ.b" ).removeClass( "active" );
    $( ".circ.c" ).removeClass( "active" );
    $( ".circ.d" ).removeClass( "active" );
    $( ".circ.e" ).removeClass( "active" );
  });
  $(".circ.b").click(function(){
    $( ".par2" ).show();
    $( ".par1" ).hide("slow");
    $( ".par3" ).hide("slow");
    $( ".par4" ).hide("slow");
    $( ".par5" ).hide("slow");
    $( ".circ.b" ).addClass( "active" );
    $( ".circ.a" ).removeClass( "active" );
    $( ".circ.c" ).removeClass( "active" );
    $( ".circ.d" ).removeClass( "active" );
    $( ".circ.e" ).removeClass( "active" );
  });
 $(".circ.c").click(function(){
    $( ".par3" ).show();
    $( ".par1" ).hide("slow");
    $( ".par2" ).hide("slow");
    $( ".par4" ).hide("slow");
    $( ".par5" ).hide("slow");
    $( ".circ.c" ).addClass( "active" );
    $( ".circ.a" ).removeClass( "active" );
    $( ".circ.b" ).removeClass( "active" );
    $( ".circ.d" ).removeClass( "active" );
    $( ".circ.e" ).removeClass( "active" );
  });
$(".circ.d").click(function(){
    $( ".par4" ).show();
    $( ".par1" ).hide("slow");
    $( ".par2" ).hide("slow");
    $( ".par3" ).hide("slow");
    $( ".par5" ).hide("slow");
    $( ".circ.d" ).addClass( "active" );
     $( ".circ.a" ).removeClass( "active" );
    $( ".circ.b" ).removeClass( "active" );
    $( ".circ.c" ).removeClass( "active" );
    $( ".circ.e" ).removeClass( "active" );
  });
$(".circ.e").click(function(){
    $( ".par5" ).show();
    $( ".par1" ).hide("slow");
    $( ".par2" ).hide("slow");
    $( ".par3" ).hide("slow");
    $( ".par4" ).hide("slow");
    $( ".circ.e" ).addClass( "active" );
     $( ".circ.a" ).removeClass( "active" );
    $( ".circ.b" ).removeClass( "active" );
    $( ".circ.d" ).removeClass( "active" );
    $( ".circ.c" ).removeClass( "active" );
  });*/
        ';
        add_jquery_ready( $js );
?>

	<div class="col-md-4 col-md-offset-1">
		<div class="project-image">
       <div class="you"><strong>YOU:</strong></div>
			<div class="iprog-circle">


               
               <img id="prog1" width="175px"  
               src="http://iprog.co/wp-content/uploads/2014/10/iProg-Buttons.png"/>
               <div class="circ a active"></div>
<p class="par1" class="teksts1"><strong>Make profit for every project we develop</strong></p>
               <img id="prog2" 
               src="http://iprog.co/wp-content/uploads/2014/10/iProg-Buttons3.png"/>
               <div class="circ b active" ></div>
<p class="par2" class="teksts1"><strong>Use our portfolio  <a style="color: rgb(255, 133, 0);
font-family: open sans,sans-serif;
font-size: 20px" href="#">www.portfolio24.co.uk<br/>(coming soon)</a></strong></p>
               <img id="prog3"
                src="http://iprog.co/wp-content/uploads/2014/10/iProg-Buttons4.png"/>
                <div class="circ c active" ></div>
<p class="par3" class="teksts1"><strong>Expand client base</strong></p>
               <img id="prog4"
                src="http://iprog.co/wp-content/uploads/2014/10/iProg-Buttons5.png"/>
                <div class="circ d active" ></div>
<p class="par4" class="teksts1"><strong>Take more challenging web projects</strong></p>
               <img id="prog5"
                width="175px" src="http://iprog.co/wp-content/uploads/2014/10/iProg-Buttons6.png"/>
                <div class="circ e active" ></div> 
<p class="par5" class="teksts1"><strong>Grow your enterprise faster</strong></p>
               

               

			</div>
            <?php if( $work_title ) { ?>
			<strong class="our-work circle"><?php echo $work_title ?></strong> 
            <?php } ?>
		</div>
	</div>
    <?php } ?> 
</div>
<style>
.footer .col-md-4{
  width: 30%!important;
}
strong.our-work.circle{
  display: none;
}
div.iprog-circle #prog1{
  position: absolute;
right:5px;
top:0;
}
div.iprog-circle #prog2{
position: absolute;
bottom: 90px;
right: -3px;
width: 145px;
}
div.iprog-circle #prog3{
width: 217px;
position: absolute;
bottom: 56px;
right: 70px;
}
div.iprog-circle #prog4{ 
 width: 145px;
position: absolute;
bottom: 90px;
left: -9px;
}
div.iprog-circle #prog5{
  position: absolute;
left:0;
top:0;
}
.par1{
  width: 300px;
border-bottom: 1px solid #000;
position: absolute;
left: 320px;
bottom: 340px;
}
.par2{
  width: 300px;
border-bottom: 1px solid #000;
position: absolute;
left: 320px;
bottom: 100px;
}
.par3{
  width: 300px;
border-bottom: 1px solid #000;
position: absolute;
left: 30px;
top: 380px;
}
.par4{
  width: 300px;
border-bottom: 1px solid #000;
position: absolute;
right: 350px;
bottom: 40px;
}
.par5{
  width: 300px;
border-bottom: 1px solid #000;
position: absolute;
right: 350px;
bottom: 350px;
}
.you{
  position: relative;
  top:170px;
  right:5px;
}

  @media (max-width: 991px) {
    .you{
      display: inline!important;
      position: relative;
      top: 30px;
    }
    #prog1,#prog2,#prog3,#prog4,#prog5{
      display: none;
    }
    div.home-concept div.project-image{
      background:none;
      height: auto;
      margin:0 auto;
      padding:0;
      position: static;
    }
    .par1,.par2,.par3,.par4,.par5{
      position: relative;
left: 10px;
right: 0;
top: -50px;
bottom: 0;
display: inline-block;
    }
    div.home-concept .par strong{
      width:300px;
      border-bottom: 1px solid #000;
    }
    .circ.a, .circ.b,.circ.c,.circ.d,.circ.e{
      position: static;
      left:0;
      right:0;
      top:20px;
    }
    .iprog-circle{
      width: 300px;
margin: 0 auto;
margin-top: 100px;
    }
  }
  @media (max-width: 480px) {
    .par1 strong,.par2 strong,.par3 strong,.par4 strong,.par5 strong{
      font-size: 20px!important; 
    }
    div.home-concept div.project-image{
    max-width: 280px;
max-height: 380px;
margin-bottom: 200px;
}

  }
  @media (max-width: 680px) {
  .wordpress.development img{
    width:300px;
  }
  #builder-module-542bd567f04a0 p{
  width:300px!important;
  }
  #builder-module-542bd567f04a0 .col-md-8{
    text-align: center;
  }
  }
</style>