<?php
  $args = array('post_type' => 'cases');
  $query = new WP_Query($args);
  while($query -> have_posts()) : $query -> the_post();
?>
<div class="case-item">
<h2 style="margin-top:20px;"><?php the_title() ?></h2>
<div class="span8 col-md-8 column_first">
    <img width="700" src="http://iprog.co/wp-content/uploads/2014/11/Macbook.png" />
    <div style="position: absolute;left: 120px;top: 25px;z-index: -1;"><?php the_post_thumbnail(); ?></div>
</div>
<div class="span4 col-md-4 column_last">
    <h3>About project</h3>
   <?php echo(types_render_field("description", array('row' => true))); ?> 
   <span class="case-category"> Category:</span>
        <?php 
echo strip_tags (
    get_the_term_list( get_the_ID(), 'case-category', "",", " ));
?>
   </span><br/>
   <span class="case-technologies"> Technolgies:</span>
        <?php 
echo strip_tags (
    get_the_term_list( get_the_ID(), 'technologies', "",", " ));
?>
   </span>
   <div class="case=proof"><a target="_blank" href="<?php echo(types_render_field("link", array('row' => true))); ?>">Link to project</a></div>
</div>
</div>
<?php endwhile; ?>



