<?php

$options = get_post_meta( get_the_ID(), '_page_options', true );
if( !empty( $options ) && isset( $options['slider'] ) ) return;
?>

<div class="header-container">
	<header class="narrow" data-after-header>
		<div class="container">
			<?php if( $logo = get_setting( 'logo', false ) ) : ?>
            <h1 class="logo">
    			<a href="<?php echo esc_url( home_url( '/' ) ) ?>">
    				<img alt="<?php bloginfo( 'name' ) ?>" width="125" height="55" src="<?php echo $logo ?>"><br/>
    				<div style="font-size: 18px;margin-bottom: 0;margin-top: -10px;color:#000;">Outsource Your Web Development</div>
    			</a>
    		</h1>
    		
    		<nav id="secondary-nav" class="menu-topmenu-container">
    			<ul id="menu-topmenu" class="nav nav-pills nav-top">
					<li class="phone" style="font-size: 16px;font-weight: bold;"><span><i class="icon icon-phone"></i> 07947625420</span></li>
					<li class="email"><span><i class="icon icon-envelope"></i><a href="mailto:info@iprog.co">info@iprog.co</span></a></li>
				</ul>
			</nav>
            <?php endif; ?>
			<button class="btn btn-responsive-nav btn-inverse" data-toggle="collapse" data-target=".nav-main-collapse">
				<i class="icon icon-bars"></i>
			</button>
		</div>
		<div class="navbar-collapse nav-main-collapse collapse">
			<div class="container">
				<?php spyropress_social_icons( 'header_social' ); ?>
				<?php
                    spyropress_get_nav_menu( array(
                        'container_class' => 'nav-main mega-menu',
                        'menu_class' => 'nav nav-pills nav-main',
                        'menu_id' => 'mainMenu'
                    ) );
                ?>
			</div>
		</div>
	</header>
</div>