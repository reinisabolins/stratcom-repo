<?php
/*
 * Core Spyropress header template
 *
 * Customise this in your child theme by:
 * - Using hooks and your own functions
 * - Using the 'header-content' template part
 * - For example 'header-content-category.php' for category view or 'header-content.php' (fallback if location specific file not available)
 * - Copying this file to your child theme and customising - it will over-ride this file
 *
 * @package Spyropress
 */
?>
<!DOCTYPE html>
<!--[if IE 8]> <html class="ie ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9]> <html class="ie ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
    <?php wp_head(); ?>
<!--<link rel='stylesheet' id='theme-css'  href='/wp-content/themes/porto/assets/css/theme.css' type='text/css' media='all' />-->
    <!-- Yandex.Metrika counter -->
    <meta name="google-site-verification" content="ycWKczPRu9lzKfNbiXmqd0DEjbnNf2GM-3il2AnVEis" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://rawgit.com/mattboldt/typed.js/master/js/typed.js"></script>
    <script type="text/javascript">
$(document).ready(function() {
    $( ".awesome input" ).addClass("awesome");
    //$(".hide1").hide();
   // $(".wpcf7-form-control-wrap.radio-722").css("margin-bottom","-80px");
    $(".awesome input").click(function (e) {
        var checked_option_radio = $('input:radio[name=radio-722]:checked').val();
        var checked_site_radio = $('input:radio[name=radio-722]:checked').val();
        
       /*if(checked_option_radio===undefined || checked_site_radio===undefined)
            {
                alert('Please select both options!');
            }else{

                alert(checked_option_radio);
            }*/

       if (checked_option_radio=='no'){
            $(".hide1").hide();
            $(".wpcf7-form-control-wrap.radio-722").css("margin-bottom","-80px");
        }
        else if (checked_option_radio=='yes'){
            $(".hide1").show();
            $(".wpcf7-form-control-wrap.radio-722").css("margin-bottom","0px");
        }
    });
});
</script>


<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter26402133 = new Ya.Metrika({id:26402133,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/26402133" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-55289955-1', 'auto');
  ga('send', 'pageview');
</script>
<script type="text/javascript">
    (function (n) {
        var u = window.location.href;
        var p = u.split("/")[0];
        var t = n.createElement("script"), i;
        t.type = "text/javascript";
        t.async = !0;
        t.src = p + "//dashboard.whoisvisiting.com/who.js";
        i = n.getElementsByTagName("script")[0];
        i.parentNode.insertBefore(t, i)
    })(document);

        var whoparam = whoparam || [];
        whoparam.push(["AcNo", "3d29ce0b67554eb8a11ef19cd33677e7"]);
        whoparam.push(["SendHit", ""]
    );
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter29327725 = new Ya.Metrika({id:29327725,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/29327725" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->


</head>
<body <?php body_class( get_setting( 'theme_layout', 'full' ) ); ?>>

<?php spyropress_wrapper(); ?>
    <!-- header -->
    <?php spyropress_before_header(); ?>
    <?php
        spyropress_before_header_content();
        $version = isset( $_GET['header'] ) ? $_GET['header'] :  get_setting( 'header_style', 'v1' );
        get_template_part( 'templates/header/header', $version );
        spyropress_after_header_content();
    ?>
    <?php spyropress_after_header(); ?>
    <!-- /header -->