# Copyright (C) 2015 GoDaddy Inc
# This file is distributed under the same license as the WP Easy Mode package.
msgid ""
msgstr ""
"Project-Id-Version: WP Easy Mode\n"
"Report-Msgid-Bugs-To: https://github.com/godaddy/wp-easy-mode/issues\n"
"POT-Creation-Date: 2015-11-16 14:58-0600\n"
"PO-Revision-Date: 2015-11-16 14:58-0600\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: ru_RU\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.6\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-KeywordsList: __;_e;__ngettext;_n;__ngettext_noop;_n_noop;_x;_nx;"
"_nx_noop;_ex;esc_attr__;esc_attr_e;esc_attr_x;esc_html__;esc_html_e;"
"esc_html_x;_c;_nc\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: assets\n"
"X-Poedit-SearchPathExcluded-1: content\n"
"X-Poedit-SearchPathExcluded-2: languages\n"
"X-Poedit-SearchPathExcluded-3: tests\n"

#: includes/class-wpem-admin.php:172
msgid "WP Easy Mode"
msgstr "Простой режим WP"

#: includes/class-wpem-admin.php:173
msgid "Easy Mode"
msgstr "Простой режим"

#: includes/class-wpem-admin.php:213
msgid "Are you sure you want to exit and configure WordPress on your own?"
msgstr ""
"Вы действительно хотите выйти из мастера и настроить WordPress "
"самостоятельно?"

#: includes/class-wpem-admin.php:249
msgid "Expand Sidebar"
msgstr "Развернуть боковую панель"

#: includes/class-wpem-admin.php:250 includes/class-wpem-step-theme.php:73
msgid "Collapse Sidebar"
msgstr "Свернуть боковую панель"

#: includes/class-wpem-customizer.php:46
msgid "Congratulations!"
msgstr "Поздравляем!"

#: includes/class-wpem-customizer.php:47
msgid ""
"You've just created your website! Use the panel to the left to customize the "
"look &amp; feel of your website's design."
msgstr ""

#: includes/class-wpem-done.php:66
msgid "Primary Menu"
msgstr "Основное меню"

#: includes/class-wpem-done.php:152
msgid "Store"
msgstr "Магазин"

#: includes/class-wpem-done.php:153
msgid "store"
msgstr "магазин"

#: includes/class-wpem-done.php:160
msgid "Cart"
msgstr "Корзина"

#: includes/class-wpem-done.php:161
msgid "cart"
msgstr "корзина"

#: includes/class-wpem-done.php:166
msgid "Checkout"
msgstr "Оформление покупки"

#: includes/class-wpem-done.php:167
msgid "checkout"
msgstr "оформление заказа"

#: includes/class-wpem-done.php:172
msgid "My Account"
msgstr "Мой аккаунт"

#: includes/class-wpem-done.php:173
msgid "account"
msgstr "аккаунт"

#: includes/class-wpem-done.php:179
msgid "About Me"
msgstr "Обо мне"

#: includes/class-wpem-done.php:180 includes/class-wpem-done.php:187
msgid "about"
msgstr "информация"

#: includes/class-wpem-done.php:186
msgid "About Us"
msgstr "О нас"

#: includes/class-wpem-done.php:194
msgid "Contact Me"
msgstr "Спросите меня"

#: includes/class-wpem-done.php:195 includes/class-wpem-done.php:202
msgid "contact"
msgstr "контакт"

#: includes/class-wpem-done.php:201
msgid "Contact Us"
msgstr "Связаться с нами"

#: includes/class-wpem-done.php:208
msgid "FAQs"
msgstr "Часто задаваемые вопросы"

#: includes/class-wpem-done.php:209
msgid "faq"
msgstr "часто задаваемые вопросы"

#: includes/class-wpem-done.php:215
msgid "Get A Quote"
msgstr "Получить предложение"

#: includes/class-wpem-done.php:216
msgid "estimates"
msgstr "оценки"

#: includes/class-wpem-done.php:222
msgid "Testimonials"
msgstr "Отзывы наших клиентов"

#: includes/class-wpem-done.php:223
msgid "testimonials"
msgstr "отзывы"

#: includes/class-wpem-done.php:415
msgid "Recent Posts"
msgstr "Последние публикации"

#: includes/class-wpem-done.php:424
msgid "Archives"
msgstr "Архивы"

#: includes/class-wpem-done.php:441
msgid "Browse Products"
msgstr "Обзор продуктов"

#: includes/class-wpem-done.php:472
msgid "Contact Form"
msgstr "Контактная форма"

#: includes/class-wpem-done.php:473 includes/class-wpem-done.php:498
msgid "Your form has been successfully submitted."
msgstr "Ваша форма успешно отправлена."

#: includes/class-wpem-done.php:474 includes/class-wpem-done.php:499
msgid "Thank you so much for contacting us. We will get back to you shortly."
msgstr ""
"Сердечно благодарим за обращение к нам. Мы свяжемся с вами в ближайшее время."

#: includes/class-wpem-done.php:497
msgid "Request A Quote Form"
msgstr "Форма запроса ценового предложения"

#: includes/class-wpem-step-settings.php:18
#: includes/class-wpem-step-settings.php:19
msgid "Settings"
msgstr "Настройки"

#: includes/class-wpem-step-settings.php:32
msgid "Please tell us more about your website (all fields are required)"
msgstr ""
"Пожалуйста, расскажите подробнее о вашем веб-сайте (все поля обязательны для "
"заполнения)"

#: includes/class-wpem-step-settings.php:35
#: includes/class-wpem-step-settings.php:111
msgid "Type"
msgstr "Тип"

#: includes/class-wpem-step-settings.php:38
msgid "- Select a type -"
msgstr "- Выберите тип -"

#: includes/class-wpem-step-settings.php:39
msgid "Standard (Recommended)"
msgstr "Стандартный (рекомендуется)"

#: includes/class-wpem-step-settings.php:40
msgid "Blog"
msgstr "Блог"

#: includes/class-wpem-step-settings.php:41
msgid "Online Store"
msgstr "Онлайн-магазин"

#: includes/class-wpem-step-settings.php:43
msgid "What type of website are you creating?"
msgstr "Какой тип веб-сайта вы создаете?"

#: includes/class-wpem-step-settings.php:47
#: includes/class-wpem-step-settings.php:117
msgid "Industry"
msgstr "Промышленность"

#: includes/class-wpem-step-settings.php:50
msgid "- Select an industry -"
msgstr "- Выберите вид деятельности -"

#: includes/class-wpem-step-settings.php:51
msgid "Business / Finance / Law"
msgstr "Бизнес / Финансы / Право"

#: includes/class-wpem-step-settings.php:52
msgid "Design / Art / Portfolio"
msgstr "Дизайн / Искусство / Портфолио"

#: includes/class-wpem-step-settings.php:53
msgid "Education"
msgstr "Образование"

#: includes/class-wpem-step-settings.php:54
msgid "Health / Beauty"
msgstr "Здоровье / Красота"

#: includes/class-wpem-step-settings.php:55
msgid "Home Services / Construction"
msgstr "Услуги на дому / Строительство"

#: includes/class-wpem-step-settings.php:56
msgid "Music / Movies / Entertainment"
msgstr "Музыка / Фильмы / Развлечения"

#: includes/class-wpem-step-settings.php:57
msgid "Non-profit / Causes / Religious"
msgstr "Некоммерческая деятельность / Ритуалы / Религия"

#: includes/class-wpem-step-settings.php:58
msgid "Other"
msgstr "Прочее"

#: includes/class-wpem-step-settings.php:59
msgid "Personal / Family / Wedding"
msgstr "Личный / Семейный / Свадебный"

#: includes/class-wpem-step-settings.php:60
msgid "Pets / Animals"
msgstr "Домашние любимцы и животные"

#: includes/class-wpem-step-settings.php:61
msgid "Real Estate"
msgstr "Недвижимость"

#: includes/class-wpem-step-settings.php:62
msgid "Restaurant / Food"
msgstr "Ресторан / Продукты питания"

#: includes/class-wpem-step-settings.php:63
msgid "Sports / Recreation"
msgstr "Спорт / Отдых"

#: includes/class-wpem-step-settings.php:64
msgid "Transportation / Automotive"
msgstr "Транспорт / Автомобили"

#: includes/class-wpem-step-settings.php:65
msgid "Travel / Hospitality / Leisure"
msgstr "Путешествия / Туризм / Отдых"

#: includes/class-wpem-step-settings.php:67
msgid "What will your website be about?"
msgstr "О чем будет ваш веб-сайт?"

#: includes/class-wpem-step-settings.php:71
#: includes/class-wpem-step-settings.php:123
msgid "Title"
msgstr "Заголовок"

#: includes/class-wpem-step-settings.php:73
msgid "Enter your website title here"
msgstr "Введите здесь название веб-сайта"

#: includes/class-wpem-step-settings.php:74
msgid ""
"The title of your website appears at the top of all pages and in search "
"results."
msgstr ""
"Название веб-сайта отображается вверху всех страниц и в результатах поиска."

#: includes/class-wpem-step-settings.php:78
#: includes/class-wpem-step-settings.php:129
msgid "Tagline"
msgstr "Подзаголовок"

#: includes/class-wpem-step-settings.php:80
msgid "Enter your website tagline here"
msgstr "Введите здесь ключевую фразу веб-сайта"

#: includes/class-wpem-step-settings.php:81
msgid ""
"Think of the tagline as a slogan that describes what makes your website "
"special. It will also appear in search results."
msgstr ""
"Подзаголовок — это лозунг, который описывает уникальность вашего веб-сайта. "
"Он также отображается в результатах поиска."

#: includes/class-wpem-step-settings.php:98
#: includes/class-wpem-step-start.php:47
msgid "Continue"
msgstr "Продолжить"

#: includes/class-wpem-step-settings.php:170
msgid "All fields are required"
msgstr "Необходимо заполнить все поля."

#: includes/class-wpem-step-start.php:18
msgid "Start"
msgstr "Старт"

#: includes/class-wpem-step-start.php:19
msgid "WordPress Setup"
msgstr "Настройка WordPress"

#: includes/class-wpem-step-start.php:32
msgid ""
"Welcome to our WordPress setup wizard. It's designed to help get your site's "
"basic configurations done quickly and easily so you can get online faster."
msgstr ""
"Вас приветствует мастер настройки WordPress. Он поможет вам быстро и легко "
"выполнить базовую настройку вашего сайта, чтобы вы смогли выйти в Интернет "
"как можно скорее."

#: includes/class-wpem-step-start.php:34
msgid "It's completely optional and will only take a few minutes."
msgstr "Эта операция необязательна и продолжается лишь несколько минут."

#: includes/class-wpem-step-start.php:46
msgid "No, thanks"
msgstr "Нет, спасибо"

#: includes/class-wpem-step-theme.php:18
msgid "Theme"
msgstr "Тема"

#: includes/class-wpem-step-theme.php:19
msgid "Choose a Theme"
msgstr "Выберите тему"

#: includes/class-wpem-step-theme.php:32
msgid ""
"Choose a design for your website (don't worry, you can change this later)"
msgstr ""
"Выберите дизайн вашего веб-сайта (вы легко сможете изменить его впоследствии)"

#: includes/class-wpem-step-theme.php:43
msgid "Preview"
msgstr "Просмотр"

#: includes/class-wpem-step-theme.php:44 includes/class-wpem-step-theme.php:64
msgid "By"
msgstr "Автор:"

#: includes/class-wpem-step-theme.php:47 includes/class-wpem-step-theme.php:59
msgid "Activate"
msgstr "Активировать"

#: includes/class-wpem-step-theme.php:56
msgid "Close"
msgstr "Закрыть"

#: includes/class-wpem-step-theme.php:57
msgid "Previous"
msgstr "Назад"

#: includes/class-wpem-step-theme.php:58
msgid "Next"
msgstr "Далее"

#: includes/class-wpem-step-theme.php:67
msgid "Version:"
msgstr "Версия:"

#: includes/class-wpem-step-theme.php:75
msgid "Collapse"
msgstr "Свернуть"

#: includes/functions.php:189
msgid "Error: WP-CLI not found"
msgstr "Ошибка: WP-CLI не найден"

#: templates/fullscreen.php:38
msgid "WordPress"
msgstr "WordPress"

#~ msgid ""
#~ "Yes, I'm interested in hiring a professional to help customize my "
#~ "WordPress site"
#~ msgstr ""
#~ "Да, я заинтересован в найме специалиста, который поможет настроить мой "
#~ "веб-сайт WordPress"
